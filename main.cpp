#include <iostream>
#include <fstream>
#include <string>
#include <random>
#include <algorithm>
#include <iomanip>

using namespace std;

struct Edge
{
	int v = -1; //������
	int m = -1; //���� 
	int weight; //���
	Edge(int v = -1, int m = -1, int w = -1) : v(v), m(m), weight(w) {}
	Edge(Edge* edge)
	{
		v = edge->v;
		m = edge->m;
		weight = edge->weight;
	}
};

struct Record
{
	Edge* edge;
	Record* next = nullptr;
	bool visited = false;
	Record() {};
	Record(Edge* edge) : edge(edge) {};
};

struct Node
{
	Node(int num) : num(num) {}
	int num; //����� �������
	bool visited = false; //�������� �� ���
};

class Graph
{
private:
	Record* lon = nullptr;
	int amofno = 0; //���������� ������
	vector<Node> arr_node;
public:
	Graph() {}
	~Graph()
	{
		Record* rec = lon;
		Record* rec2 = lon;
		while (rec != nullptr)
		{
			rec2 = rec;
			rec = rec->next;
			delete(rec2);
		}
	}
	void insert(Edge* edge);
	void print();
	void setAmofno(int ch);
	void add_arr_node(Node* node);
	void depthSearch(int m);
	void depthSearchGraph(int m);
	void Kraskala();
	int search_arr(int m);
	bool search_ed(int n, int k);
	void clean();
	int get_amofno();
};


int Graph::get_amofno()
{
	return amofno;
}

void Graph::clean()
{
	for (int i = 0; i < arr_node.size(); i++)
	{
		arr_node[i].visited = false;
	}
}


int Graph::search_arr(int m)
{
	for (int i = 0; i < amofno; i++)
	{
		if (arr_node[i].num == m)
		{
			return (i);
		}
	}
	return -1;
}

bool Graph::search_ed(int n, int k)
{
	bool found = false;
	Record* rec = lon;
	while ((rec != nullptr) && (!found))
	{
		if ((rec->edge->v == n) && (rec->edge->m == k))
		{
			return true;
		}
		rec = rec->next;
	}
	if (!found)
	{
		return false;
	}
}

void Graph::setAmofno(int ch)
{
	amofno = ch;
}

void Graph::add_arr_node(Node* node)
{
	arr_node.push_back(*node);
	amofno = amofno + 1;
}

void Graph::insert(Edge* edge)
{
	Record* cur = lon;
	Record* cur2 = nullptr;
	if (cur != nullptr)
	{
		while (cur != nullptr && cur->edge->weight <= edge->weight)
		{
			cur2 = cur;
			cur = cur->next;
		}
		if (cur == nullptr) //����� � �����
		{
			Record* new_rec = new Record(edge);
			cur2->next = new_rec;
			new_rec->next = nullptr;
		}
		else
		{
			Record* new_rec = new Record(edge);
			if (cur2 == nullptr)
			{
				lon = new_rec;
			}
			else
			{
				cur2->next = new_rec;
			}
			new_rec->next = cur;
		}
	}
	else //������ ����
	{
		Record* new_rec = new Record(edge);
		lon = new_rec;
	}
}


void Graph::print()
{
	Record* rec = lon;
	while (rec != nullptr)
	{
		cout << setw(2) << rec->edge->v << " " << setw(2) << rec->edge->m << " | " << rec->edge->weight << endl;
		rec = rec->next;
	}
	cout << endl;
}

void Graph::depthSearch(int m)
{
	if (search_arr(m) != -1)
	{
		arr_node[search_arr(m)].visited = true;
		cout << m << " ";
		for (int i = 0; i < amofno; i++)
		{
			if (arr_node[i].visited != true) //���� �� �� �������� i �������
			{
				bool found = false;
				Record* rec = lon;
				while ((rec != nullptr) && (!found))
				{
					if ((rec->edge->v == m) && (rec->edge->m == arr_node[i].num) || (rec->edge->v == arr_node[i].num) && (rec->edge->m == m)) //���� �����
					{
						found = true;
					}
					rec = rec->next;
				}
				if (found)
				{
					depthSearch(arr_node[i].num);
				}
			}
		}
	}
}

void Graph::depthSearchGraph(int m)
{
	depthSearch(m);
	clean();
}


void readFile(ifstream& fi, Graph* graph)
{
	string temp = "";
	string num;
	getline(fi, temp);
	int ch1, ch2, ch3, ch4;
	auto iter = temp.begin();
	auto iter2 = temp.end();
	while (*iter != ' ')
	{
		num = num + *iter;
		iter = iter + 1;
	}
	iter = iter + 1;
	ch1 = stoi(num); //���������� �����
	num = "";
	while (iter != iter2)
	{
		num = num + *iter;
		iter = iter + 1;
	}
	ch1 = stoi(num); //���������� ������
	int ech1 = ch1;
	num = "";
	getline(fi, temp); //���������� ������
	iter = temp.begin();
	iter2 = temp.end();
	while (iter != iter2)
	{
		if (*iter == ' ')
		{
			ch1 = stoi(num);
			num = "";
			Node* node = new Node(ch1);
			graph->add_arr_node(node);
		}
		else
		{
			num = num + *iter;
		}
		iter = iter + 1;
	}
	ch1 = stoi(num);
	Node* node = new Node(ch1);
	graph->add_arr_node(node);
	num = "";
	for (int i = 0; i < ech1; i++)
	{
		getline(fi, temp);
		iter = temp.begin();
		iter2 = temp.end();
		while (*iter != ' ')
		{
			num = num + *iter;
			iter = iter + 1;
		}
		ch2 = stoi(num); //���� 1
		num = "";
		iter = iter + 1;
		while (*iter != ' ')
		{
			num = num + *iter;
			iter = iter + 1;
		}
		ch3 = stoi(num); //���� 2
		num = "";
		while (iter != iter2)
		{
			num = num + *iter;
			iter = iter + 1;
		}
		ch4 = stoi(num); //���
		num = "";
		Edge* edge = new Edge(ch2, ch3, ch4);
		graph->insert(edge);
	}
}

void Graph::Kraskala()
{
	Record* rec = lon;
	Record* rec_min = lon;
	vector<int> arrofn;
	cout << setw(2) << rec->edge->m << " " << setw(2) << rec->edge->v << " " << "| " << rec->edge->weight << endl;
	arrofn.push_back(rec->edge->m);
	arrofn.push_back(rec->edge->v);
	int amofno2 = 2;
	//rec = rec->next;
	rec_min = rec;
	bool found = false;
	bool ng = false;
	while (amofno != amofno2 && !ng) //���� �� ����� ��� ������� 
	{
		rec = rec_min;
		//���� �� ����� �� ������� ��� ���� �� ����� ���������� �����
		while ((rec->next != nullptr) && !found)
		{
			rec = rec->next;
			if (rec->visited)
			{
				if (rec == rec_min)
				{
					rec_min = rec->next;
				}
			}
			else
			{
				if (find(arrofn.begin(), arrofn.end(), rec->edge->v) != arrofn.end() || find(arrofn.begin(), arrofn.end(), rec->edge->m) != arrofn.end())
				{
					found = true;
				}
			}
		}
		if (rec->next == nullptr) //��� ������ �� �������
		{
			ng = true;
		}
		else
		{
			found = false;
			if (!rec->visited)
			{
				if (rec == nullptr) //���� ���� � �����
				{
					rec = rec_min;
				}
				else
				{
					//���� ����� �������� ����
					if ((find(arrofn.begin(), arrofn.end(), rec->edge->v) != arrofn.end()) && (find(arrofn.begin(), arrofn.end(), rec->edge->m)) != arrofn.end())
					{
						rec->visited = true;
						if (rec_min == rec)
						{
							rec_min == rec->next; //���� ��������� �����, �� �������
						}
					}
					else
					{
						cout << setw(2) << rec->edge->m << " " << setw(2) << rec->edge->v << " " << "| " << rec->edge->weight << endl;
						if (find(arrofn.begin(), arrofn.end(), rec->edge->v) != arrofn.end())
						{
							arrofn.push_back(rec->edge->m);
						}
						else
						{
							arrofn.push_back(rec->edge->v);
						}
						rec->visited = true;
						amofno2 = amofno2 + 1;
						if (rec_min->next == rec)
						{
							rec_min = rec;
						}
						rec = rec_min;
					}
				}
			}
		}
	}
	Record* recc = new Record();
	recc = lon;
	while (recc != nullptr)
	{
		recc->visited = false;
		recc = recc->next;
	}
}

int main()
{
	ifstream input_file;
	input_file.open("graph1.txt");
	Graph* graph1 = new Graph();
	readFile(input_file, graph1);
	graph1->print();
	graph1->depthSearchGraph(1);
	cout << endl;
	//graph1->depthSearchGraph(2);
	//cout << endl;
	graph1->Kraskala();
	input_file.close();
	return 0;
}